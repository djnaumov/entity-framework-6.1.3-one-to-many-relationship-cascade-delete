﻿using System;
using System.Data.Entity;
using System.Linq;

namespace TestEF
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MyContext>());

                using (var db = new MyContext())
                {
                    Console.WriteLine("-Add- (Add new person Vasya)");
                    Add(db);
                    View(db);

                    Console.WriteLine("-Update- (Vasya upload new photo)");
                    Update(db);
                    View(db);

                    Console.WriteLine("-Delete- (Remove Vasya)");
                    Delete(db);
                    View(db);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Error: {0}", exception.Message);
            }
            Console.WriteLine("Press any key.");
            Console.ReadKey();
        }

        private static void Delete(MyContext db)
        {
            var person = db.People.FirstOrDefault(x => x.Name == "Vasya");
            db.Entry(person).Collection(x => x.Photos).Load();
            db.People.Remove(person);
            db.SaveChanges();
        }

        private static void Update(MyContext db)
        {
            var person = db.People.FirstOrDefault(x => x.Name == "Vasya");
            db.Entry(person).Collection(x => x.Photos).Load();
            if (person != null) person.Photos.Add(new Photo {Path = "my new avatar"});
            db.SaveChanges();
        }

        private static void View(MyContext db)
        {
            var people = db.People.Include(x => x.Photos).ToList();
            Console.WriteLine("# People : {0} #", people.Count);
            foreach (Person person in people)
            {
                Console.WriteLine(person);
            }
            Console.WriteLine();

            var photos = db.Photos.Include(x => x.Person).ToList();
            Console.WriteLine("# Photos : {0} #", photos.Count);
            foreach (Photo photo in db.Photos)
            {
                Console.WriteLine("{0} owner is {1}", photo, photo.Person.Name);
            }
            Console.WriteLine();
        }

        private static void Add(MyContext db)
        {
            Person person = new Person
            {
                Name = "Vasya",
            };
            db.People.Add(person);
            db.SaveChanges();
        }
    }
}
