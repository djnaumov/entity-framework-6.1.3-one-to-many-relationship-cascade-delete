﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace TestEF
{
    internal class MyContext : DbContext
    {
        public MyContext() : base("MyContext")
        {
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Photo> Photos { get; set; }
    }

    internal class Person
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 PersonId { get; set; }

        public String Name { get; set; }
        public ICollection<Photo> Photos { get; set; }

        public override string ToString()
        {
            return String.Format("{0} have photos {1}.", Name, Photos.Count);
        }
    }

    internal class Photo
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }

        public String Path { get; set; }
        public Int32 PersonId { get; set; }
        public Person Person { get; set; }

        public override string ToString()
        {
            return String.Format("{0}", Path);
        }
    }
}
